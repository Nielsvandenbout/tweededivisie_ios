//
//  SaldoWebViewController.swift
//  HeraclesAlmelo
//
//  Created by Niels on 25-05-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit

class SaldoWebViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    var url: String!
    var nsUrl: NSURL!
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews() {
        webView.delegate = self
        nsUrl = NSURL(string: url)
        let request = NSURLRequest(URL: nsUrl!)
        webView.loadRequest(request)
        activityIndicator.startAnimating()
        activityIndicator.center.y = self.view.center.y - 44
        activityIndicator.center.x = self.view.center.x
        view.addSubview(activityIndicator)
        
        // Center
        let imageView = UIImageView(image: UIImage(named: "businessclub.png"))
        imageView.frame.size.height = 20
        imageView.contentMode = UIViewContentMode.ScaleAspectFit
        
        navigationController?.navigationBar.translucent = false
        self.navigationItem.titleView = imageView
        let barButton = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: #selector(openInSafari))
        navigationItem.rightBarButtonItem = barButton

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
    }

    func webViewDidFinishLoad(webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        captureSession?.startRunning()
    }

    func openInSafari() {
        let activity = TUSafariActivity()
        let activityViewController = UIActivityViewController(activityItems: [nsUrl], applicationActivities: [activity])
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
}
