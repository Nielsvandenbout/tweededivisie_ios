//
//  SaldoCheckerViewController.swift
//  HeraclesAlmelo
//
//  Created by Niels on 25-05-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit

import AVFoundation


func delay(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time( DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), closure)
}

func showSnackbar(title: String, color: String, superView: UIView, hasNavigationBar: Bool) {
    let snackbar = UIView()
    let titleLabel = UILabel()
    
    titleLabel.text = title
    snackbar.frame.size.width = UIScreen.mainScreen().bounds.size.width
    titleLabel.frame.size.width = UIScreen.mainScreen().bounds.size.width
    titleLabel.frame.size.height = 44
    
    titleLabel.textAlignment = .Center
    titleLabel.textColor = UIColor.whiteColor()
    titleLabel.font = UIFont(name: "DINCond-MediumAlternate", size: 20)
    if hasNavigationBar == false {
        snackbar.frame.size.height = 64
        snackbar.center.y = -32
        titleLabel.center.y = 40
    } else {
        snackbar.frame.size.height = 44
        snackbar.center.y = -44
        titleLabel.center.y = 22
    }
    snackbar.center.x = UIScreen.mainScreen().bounds.size.width / 2
    
    
    if color == "Orange" {
        snackbar.backgroundColor = UIColor.orangeColor()
    } else if color == "Red" {
        snackbar.backgroundColor = UIColor.redColor()
    } else if color == "Green" {
        snackbar.backgroundColor = UIColor(red: 0/256, green: 182/256, blue: 0/256, alpha: 1.0)
    }
    
    
    
    snackbar.addSubview(titleLabel)
    superView.addSubview(snackbar)
    
    UIView.animateWithDuration(0.4) {
        snackbar.layer.position.y += 64
    }
    
    delay(2.0) {
        UIView.animateWithDuration(0.4, animations: {
            snackbar.layer.position.y -= 64
        })
    }
}


class SaldoCheckerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var allowButton: UIButton!
    @IBOutlet weak var permissionLabel: UILabel!
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var url: String!
    var urlBool: Bool!
    var noUrl = "Geen QR code gedetecteerd."
    var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    var urlArray: [String]!
    
    let supportedBarCodes = [AVMetadataObjectTypeQRCode, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeUPCECode, AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeAztecCode]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cameraMediaType = AVMediaTypeVideo
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(cameraMediaType)
        switch cameraAuthorizationStatus {
            
        // The client is authorized to access the hardware supporting a media type.
        case .Authorized:
            print("authorized")
            permissionLabel.hidden = true
            allowButton.hidden = true
            setupCaptureView()
        case .Restricted:
            print("restricted")
            permissionLabel.text = "U heeft niet de rechten om de camera te gebruiken."
            permissionLabel.hidden = false
            allowButton.hidden = false
        // The user explicitly denied access to the hardware supporting a media type for the client.
        case .Denied:
            print("denied")
            permissionLabel.hidden = false
            allowButton.hidden = false
        // Indicates that the user has not yet made a choice regarding whether the client can access the hardware.
        case .NotDetermined:
            setupCaptureView()
            permissionLabel.hidden = true
            allowButton.hidden = true
        }
        
        captureSession?.startRunning()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        urlBool = false
        videoPreviewLayer?.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        
    }

    @IBAction func askForPermisson(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
    }
    
    func setupCaptureView() {
        if let inputs = captureSession!.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                captureSession!.removeInput(input)
            }
        }
        if let outputs = captureSession!.outputs as? [AVCaptureOutput]  {
            for output in outputs {
                captureSession?.removeOutput(output)
            }
        }

        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession?.addInput(input)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
            captureMetadataOutput.metadataObjectTypes = supportedBarCodes
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            captureSession?.startRunning()
            qrCodeFrameView = UIView()
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor(red: 0, green: 0.62, blue: 0.91, alpha: 1).CGColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubviewToFront(qrCodeFrameView)
                view.bringSubviewToFront(bottomView)
            }
        } catch {
            return
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.whiteColor()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        if metadataObjects == nil || metadataObjects.count == 0 {
            urlBool = false
            qrCodeFrameView?.frame = CGRectZero
            return
        }
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        if supportedBarCodes.contains(metadataObj.type) {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metadataObj)
            qrCodeFrameView?.hidden = false
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                let stringWithPossibleURL: String = metadataObj.stringValue
                if var validURL: NSURL = NSURL(string: stringWithPossibleURL) {
                    captureBool = true
                    if captureBool == true {
                        captureSession?.stopRunning()
                        url = metadataObj.stringValue
                        if url != "" || url != nil {
                            if urlArray == nil {
                                urlArray = [stringWithPossibleURL]
                            } else {
                                urlArray.insert(stringWithPossibleURL, atIndex: 0)
                            }
                            print(urlArray)
                            NSUserDefaults.standardUserDefaults().setObject(urlArray, forKey: "url")
                            delay(0.7) {
                                UIApplication.sharedApplication().openURL(validURL)
                                self.qrCodeFrameView?.hidden = true
                                delay(0.2, closure: {
//                                    self.messageLabel.text = self.noUrl
                                })
                                
                            }
                        } else {
                            validURL = NSURL(string: "http://\(stringWithPossibleURL)")!
                            if urlArray == nil {
                                urlArray = ["http://\(stringWithPossibleURL)"]
                            } else {
                                urlArray.insert("http://\(stringWithPossibleURL)", atIndex: 0)
                            }
                            
                            NSUserDefaults.standardUserDefaults().setObject(urlArray, forKey: "url")
                            delay(0.7) {
                                UIApplication.sharedApplication().openURL(validURL)
                                self.qrCodeFrameView?.hidden = true
                                delay(0.2, closure: {
//                                    self.messageLabel.text = self.noUrl
                                })
                            }
                        }
                    }
                }
            }
        }
    }
//                            debugPrint(validURL)
//                            if url.lowercaseString.rangeOfString("https://heracles.mijnbetaalkaart.nl") != nil {
//                                delay(0.7) {
//                                    self.performSegueWithIdentifier("showWebView", sender: self)
//                                    self.qrCodeFrameView?.hidden = true
//                                }
//                            } else {
//                                captureSession?.stopRunning()
//                                showSnackbar("Geen betaalkaart", color: "Orange", superView: self.view, hasNavigationBar: true)
//                                delay(0.5, closure: { 
//                                    captureSession?.startRunning()
//                                    self.qrCodeFrameView?.hidden = true
//                                })
//                            }
//                        }
//                    }
//                } else {
//                    captureSession?.stopRunning()
//                    showSnackbar("Geen geldige URL", color: "Orange", superView: self.view, hasNavigationBar: true)
//                    delay(0.5, closure: {
//                        captureSession?.startRunning()
//                        self.qrCodeFrameView?.hidden = true
//                    })
//                }
//            }
//        }
//    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destinationVC = segue.destinationViewController as! SaldoWebViewController
        if segue.identifier == "showWebView" {
            destinationVC.url = url
            
        } else if segue.identifier == "showHeracles" {
            destinationVC.url = "https://heracles.mijnbetaalkaart.nl/"
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        captureSession?.stopRunning()
    }
}
