//
//  AppDelegate.swift
//  QR Scanner
//
//  Created by Niels van den Bout on 06-10-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit
import AVFoundation

var captureBool = false
var captureSession:AVCaptureSession?
var messageText: String!

public extension UIView {
    
    func fadeIn(duration duration: NSTimeInterval = 0.5) {
        UIView.animateWithDuration(duration, animations: {
            self.alpha = 1.0
        })
    }
    
    func fadeOut(duration duration: NSTimeInterval = 0.5) {
        UIView.animateWithDuration(duration, animations: {
            self.alpha = 0.0
        })
    }
    
}

var activityIndicator: CustomActivityIndicatorView = {
    let image: UIImage = UIImage(named: "ball")!
    return CustomActivityIndicatorView(image: image)
}()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window?.tintColor = UIColor(colorLiteralRed: 181/256, green: 0/256, blue: 55/256, alpha: 1.0)
        UINavigationBar.appearance().barStyle = .Black
        UINavigationBar.appearance().barTintColor =  UIColor(colorLiteralRed: 181/256, green: 0/256, blue: 55/256, alpha: 1.0)
        UINavigationBar.appearance().translucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Avenir", size: 20)!]
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        captureSession = AVCaptureSession()
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        captureSession?.stopRunning()
        messageText = "Geen QR code gedetecteerd."
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        captureSession?.startRunning()
        messageText = "Geen QR code gedetecteerd."
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}

