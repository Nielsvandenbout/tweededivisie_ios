//
//  HistoryViewController.swift
//  Tap-Me
//
//  Created by Niels van den Bout on 23-06-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var urlArray: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let userDefaults = NSUserDefaults.standardUserDefaults().arrayForKey("url") {
            for item in userDefaults {
                if urlArray == nil {
                    urlArray = [item as! String]
                } else {
                    urlArray.insert(item as! String, atIndex: 0)
                }
                
            }
        }
        if urlArray == nil {
            urlArray = ["Er is nog geen geschiedenis"]
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func clearAll(sender: AnyObject) {
        let optionAlert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        optionAlert.addAction(UIAlertAction(title: "Verwijder geschiedenis", style: .Default, handler: { (UIAlertAction) in
            self.urlArray = ["Er is nog geen geschiedenis"]
            NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "url")
            self.tableView.reloadData()
            
        }))
        optionAlert.addAction(UIAlertAction(title: "Annuleer", style: .Cancel, handler: nil))
        presentViewController(optionAlert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(urlArray.count)
       return urlArray.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if urlArray.first != "Er is nog geen geschiedenis" {
            UIApplication.sharedApplication().openURL(NSURL(string: urlArray[indexPath.row])!)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! HistoryTableViewCell
        
        cell.urlLabel.text = urlArray[indexPath.row]
        if urlArray.first == "Er is nog geen geschiedenis" {
            cell.urlLabel.textAlignment = .Center
            cell.accessoryType = UITableViewCellAccessoryType.None
            tableView.separatorColor = UIColor.whiteColor()
        }
        print(urlArray[indexPath.row])
        
        return cell
    }
    
    @IBAction func back(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
        captureSession?.startRunning()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
