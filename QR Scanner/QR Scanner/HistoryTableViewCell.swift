//
//  HistoryTableViewCell.swift
//  Tap-Me
//
//  Created by Niels van den Bout on 23-06-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var urlLabel: UILabel!
    var url: NSURL!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
