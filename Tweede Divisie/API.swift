//
//  API.swift
//  Steekmaat
//
//  Created by Niels van den Bout on 06-09-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    class var sharedInstance: API {
        struct Singleton {
            static let instance = API()
        }
        return Singleton.instance
    }
    
    var data = [DataItems]()
    
    func getNews(completionHandler: (responseObject: ResponseClass?, error: NSError?) -> ()) {
        Alamofire.request(Config.Router.GetNews).responseObject { (response: Response<ResponseClass, NSError>) in
            completionHandler(responseObject: response.result.value, error: response.result.error)
            if response.result.value != nil {
                self.data = (response.result.value?.response?.dataItems)!
            }
        }
    }
    
    func getRanking(completionHandler: (responseObject: ResponseClass?, error: NSError?) -> ()) {
        Alamofire.request(Config.Router.GetRanking).responseObject { (response: Response<ResponseClass, NSError>) in
            completionHandler(responseObject: response.result.value, error: response.result.error)
        }
    }
    
    func getMatches(completionHandler: (responseObject: [Game]?, error: NSError?) ->()) {
        Alamofire.request(.GET, "https://api.crowdscores.com/api/v1/matches?competition_id=330&api_key=03c8676748604bb19882b75fa214b3f7")
            .responseCollection { (response: Response<[Game], NSError>) in
            completionHandler(responseObject: response.result.value, error: response.result.error)
        }
    }
    
}

extension String {
    func chopPrefix(count: Int = 1) -> String {
        return self.substringFromIndex((self.startIndex.advancedBy(count)))
    }
    
    func chopSuffix(count: Int = 1) -> String {
        return self.substringToIndex((self.endIndex.advancedBy(-count)))
    }
}

extension UIView {
    func fadeIn(time: Double, alpha: CGFloat) {
        // Move our fade out code from earlier
        UIView.animateWithDuration(time, delay: 0.25, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.alpha = alpha// Instead of a specific instance of, say, birdTypeLabel, we simply set [thisInstance] (ie, self)'s alpha
            }, completion: nil)
    }
    
    func fadeOut(time: Double, alpha: CGFloat) {
        UIView.animateWithDuration(time, delay: 0.25, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.alpha = alpha
            }, completion: nil)
    }
}
