//
//  GameViewController.swift
//  Tweede Divisie
//
//  Created by Niels van den Bout on 19-09-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit
import Haneke
import TwitterKit
import Firebase

class GameViewController: UIViewController {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var soccerField: UIView!
    @IBOutlet weak var awayTeam: UILabel!
    @IBOutlet weak var homeTeam: UILabel!
    @IBOutlet weak var awayImage: UIImageView!
    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var liveScore: UILabel!
    


    
    var game = Game()
    var teamsItems = [DataItems]()
    var scoreTableItems = [LiveGameItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        let hometeamName = setTeamName(game!.homeTeam!.name!)
        let awayteamName = setTeamName(game!.awayTeam!.name!)
        homeTeam.text = hometeamName
        awayTeam.text = awayteamName
        setTeamImages(homeImage, superViewTwo: awayImage, teams: [hometeamName, awayteamName])
        liveScore.text = "\(game!.homeGoals!):\(game!.awayGoals!)"
        

    }


    
    func setTeamName(name: String) -> String {
        if name == "BVV Barendrecht" {
            return "Barendrecht"
        } else if name == "Lienden" {
            return "FC Lienden"
        } else if name == "Jong Sparta Rotterdam" {
            return "Jong Sparta"
        } else if name == "Amsterdamsche" {
            return "AFC"
        } else if name == "Jong AZ Alkmaar" {
            return "Jong AZ"
        } else if name == "Jong Twente" {
            return "Jong FC Twente"
        } else if name == "Koninklijke" {
            return "Koninklijke HFC"
        } else {
            return name
        }
    }
    
    func setTeamImages(superViewOne: UIImageView, superViewTwo: UIImageView, teams: [String]) {
        for team in teamsItems {
            if team.teamName != nil {
                if team.teamName == teams[0] {
                    if let url = team.attachments?.images.first?.original {
                        showImage(url, imageView: superViewOne, contentMode: .ScaleAspectFit)
                    }
                }
                if teams.count == 2 {
                    if team.teamName == teams[1] {
                        if let url = team.attachments?.images.first?.original {
                            showImage(url, imageView: superViewTwo, contentMode: .ScaleAspectFit)
                        }
                    }
                }
            }
        }
    }
    
    
    func setupView() {
        homeTeam.text = game?.homeTeam?.name
        awayTeam.text = game?.awayTeam?.name
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "action_button"), style: .Plain, target: self, action: #selector(showAction))
    }
    
    func showAction() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        alertController.addAction(UIAlertAction(title: "Stand delen", style: .Default, handler: { (UIAlertAction) in
            
        }))
        alertController.addAction(UIAlertAction(title: "Annuleer", style: .Cancel, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showImage(url: String, imageView: UIImageView, contentMode: UIViewContentMode) {
        imageView.hnk_setImageFromURL(NSURL(fileURLWithPath: url))
        let URL = NSURL(string: url)!
        
        let cache = Shared.imageCache
        let fetcher = NetworkFetcher<UIImage>(URL: URL)
        cache.fetch(fetcher: fetcher).onSuccess { image in
            imageView.image = image
            imageView.contentMode = contentMode
            imageView.clipsToBounds = true
        }
        cache.fetch(fetcher: fetcher).onFailure({ (error) in
            
        })
    }
}

class LiveGameCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var leftMinute: UILabel!
    @IBOutlet weak var rightMinute: UILabel!
    @IBOutlet weak var leftLine: UIView!
    @IBOutlet weak var rightLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
