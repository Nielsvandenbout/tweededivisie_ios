//
//  SetNotificationsViewController.swift
//  Tweede Divisie
//
//  Created by Niels van den Bout on 19-09-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit
import Haneke

class SetNotificationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var notiView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var teamsItems = [DataItems]()
    var checked = [Bool]()
    var favTeams = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func downloadTeamsData() {
        API.sharedInstance.getRanking { (responseObject, error) in
            if responseObject != nil {
                if responseObject?.response != nil {
                    if responseObject?.response?.dataItems != nil {
                        self.teamsItems.removeAll()
                        for team in (responseObject?.response?.dataItems)! {
                            self.teamsItems.append(team)
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func setupViews() {
        setupNotiView()
        setupSaveButton()
        setupTable()
        if teamsItems.count == 0 {
            downloadTeamsData()
        }

    }
    
    func setupTable() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setupNotiView() {
        notiView.layer.cornerRadius = 22
        notiView.clipsToBounds = true
    }
    
    func setupSaveButton() {
        saveButton.layer.cornerRadius = 22
    }
    
    func addPersonsToArray(team: String) {
        favTeams.append(team)
    }
    
    func removePersonsFromArray(removedTeam: String) {
        
        for team in favTeams {
            if team == removedTeam {
                favTeams.removeAtIndex(favTeams.indexOf(removedTeam)!)
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let savedChecklist =  NSUserDefaults.standardUserDefaults().arrayForKey("checkedArray") {
            if checked.count == teamsItems.count {
                checked = savedChecklist as! [Bool]
            } else {
                for _ in teamsItems {
                    checked.append(false)
                }
            }
        } else {
            for _ in teamsItems {
                checked.append(false)
            }
        }

        return teamsItems.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            if cell.accessoryType == .Checkmark {
                cell.accessoryType = .None
                removePersonsFromArray(teamsItems[indexPath.row].teamId!)
                checked[indexPath.row] = false
            } else {
                cell.accessoryType = .Checkmark
                addPersonsToArray(teamsItems[indexPath.row].teamId!)
                checked[indexPath.row] = true
            }
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("notificationTeamCell") as! NotificationTeamCell
        cell.teamName.text = teamsItems[indexPath.row].teamName
        showImage((teamsItems[indexPath.row].attachments?.images.first?.original)!, imageView: cell.teamImage, contentMode: .ScaleAspectFit)
        if !checked[indexPath.row] {
            cell.accessoryType = .None
        } else if checked[indexPath.row] {
            cell.accessoryType = .Checkmark
        }
        return cell
    }
    
    func showImage(url: String, imageView: UIImageView, contentMode: UIViewContentMode) {
        imageView.hnk_setImageFromURL(NSURL(fileURLWithPath: url))
        let URL = NSURL(string: url)!
        
        let cache = Shared.imageCache
        let fetcher = NetworkFetcher<UIImage>(URL: URL)
        cache.fetch(fetcher: fetcher).onSuccess { image in
            imageView.image = image
            imageView.contentMode = contentMode
            imageView.clipsToBounds = true
        }
        cache.fetch(fetcher: fetcher).onFailure({ (error) in
            
        })
        
    }

    @IBAction func saveNotificationPrefs(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().setObject(checked, forKey: "checkedArray")
        dismissViewControllerAnimated(true, completion: nil)
    }
}

class NotificationTeamCell: UITableViewCell {
    
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var teamName: UILabel!
}
