//
//  Models.swift
//  Tweede Divisie
//
//  Created by Niels van den Bout on 14-09-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import Foundation

final class ResponseClass: ResponseObjectSerializable {
    
    var response: DataResponse?
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let responseObject = representation.valueForKeyPath("response") {
            self.response = DataResponse(response: response, representation: responseObject)
        } else {
            self.response = nil
        }
    }
}

final class DataResponse: ResponseObjectSerializable {
    var status: String!
    var dataItems: [DataItems]!
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let statusObject = representation.valueForKeyPath("status") as? String {
            self.status = statusObject
        } else {
            self.status = nil
        }
        if let dataItemsObject: AnyObject = representation.valueForKeyPath("dataItems") {
            self.dataItems = DataItems.collection(response: response, representation: dataItemsObject)
        } else {
            self.dataItems = nil
        }
    }
}

final class LiveGameItem {
    var leftRight: Bool
    var image: String
    var name: String
    var minute: String
    
    init(leftRight: Bool, image: String, name: String, minute: String) {
        self.leftRight = leftRight
        self.image = image
        self.name = name
        self.minute = minute
    }
}

final class DataItems: ResponseObjectSerializable {
    var id: String!
    var title: String!
    var author: String!
    var content: String!
    var tags: String!
    var publishDate: String!
    var attachments: Attachments?
    var teamId: String!
    var teamName: String!
    var rank: String!
    var played: String!
    var won: String!
    var draw: String!
    var lost: String!
    var goalsScored: String!
    var goalsAgainst: String!
    var goalsDifference: String!
    var points: String!
    var rankDifference: String!
    
    init?() {
        
    }
    init?(played: String, won: String, draw: String, lost: String, goalsScored: String, goalsAgainst: String, goalsDifference: String, points: String, rank: String, rankDifference: String) {
        self.played = played
        self.won = won
        self.draw = draw
        self.lost = lost
        self.goalsScored = goalsScored
        self.goalsAgainst = goalsAgainst
        self.goalsDifference = goalsDifference
        self.points = points
        self.rank = rank
        self.rankDifference = rankDifference
    }
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let idObject = representation.valueForKeyPath("id") as? String {
            self.id = idObject
        } else {
            self.id = nil
        }
        if let tagsObject = representation.valueForKeyPath("tags") as? String {
            self.tags = tagsObject
        } else {
            self.tags = nil
        }
        if let titleObject = representation.valueForKeyPath("title") as? String {
            self.title = titleObject
        } else {
            self.title = nil
        }
        if let authorObject = representation.valueForKeyPath("author") as? String {
            self.author = authorObject
        } else {
            self.author = nil
        }
        if let contentObject = representation.valueForKeyPath("content") as? String {
            self.content = contentObject
        } else {
            self.content = nil
        }
        if let publishDateObject = representation.valueForKeyPath("publish_date") as? String {
            self.publishDate = publishDateObject
        } else {
            self.publishDate = nil
        }
        if let attachmentsObject = representation.valueForKeyPath("attachments") {
            self.attachments = Attachments(response: response, representation: attachmentsObject)
        } else {
            self.attachments = nil
        }
        if let teamIdObject = representation.valueForKeyPath("id") as? String {
            self.teamId = teamIdObject
        } else {
            self.teamId = nil
        }
        if let teamNameObject = representation.valueForKeyPath("name") as? String {
            self.teamName = teamNameObject
        } else {
            self.teamName = nil
        }
        if let rankObject = representation.valueForKeyPath("rank") as? String {
            self.rank = rankObject
        } else {
            self.rank = nil
        }
        if let playedObject = representation.valueForKeyPath("played") as? String {
            self.played = playedObject
        } else {
            self.rank = nil
        }
        if let wonObject = representation.valueForKeyPath("won") as? String {
            self.won = wonObject
        } else {
            self.won = nil
        }
        if let drawObject = representation.valueForKeyPath("draw") as? String {
            self.draw = drawObject
        } else {
            self.draw = nil
        }
        if let lostObject = representation.valueForKeyPath("lost") as? String {
            self.lost = lostObject
        } else {
            self.lost = nil
        }
        if let goalsScoredObject = representation.valueForKeyPath("goals_scored") as? String {
            self.goalsScored = goalsScoredObject
        } else {
            self.goalsScored = nil
        }
        if let goalsAgainstObject = representation.valueForKeyPath("goals_against") as? String {
            self.goalsAgainst = goalsAgainstObject
        } else {
            self.goalsAgainst = nil
        }
        if let goalsDifferenceObject = representation.valueForKeyPath("goals_difference") as? String {
            self.goalsDifference = goalsDifferenceObject
        } else {
            self.goalsDifference = nil
        }
        if let pointsObject = representation.valueForKeyPath("points") as? String {
            self.points = pointsObject
        } else {
            self.points = nil
        }
        if let rankDifferenceObject = representation.valueForKeyPath("rank_difference") as? String {
            self.rankDifference = rankDifferenceObject
        } else {
            self.rankDifference = nil
        }
    }
    
    internal static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [DataItems] {
        var objects: [DataItems] = []
        if let representation = representation as? [[String: AnyObject]] {
            for userRepresentation in representation {
                if let item = DataItems(response: response, representation: userRepresentation) {
                    objects.append(item)
                }
            }
        }
        return objects
    }
    
}

final class Attachments: ResponseObjectSerializable {
    var images: [Images]!
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let imagesObject: AnyObject = representation.valueForKeyPath("images") {
            self.images = Images.collection(response: response, representation: imagesObject)
        } else {
            self.images = nil
        }
    }
}

final class Images: ResponseObjectSerializable, ResponseCollectionSerializable {
    var original: String!
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let originalObject = representation.valueForKeyPath("original") as? String {
            self.original = originalObject
        } else {
            self.original = nil
        }
    }
    
    internal static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Images] {
        var objects: [Images] = []
        if let representation = representation as? [[String: AnyObject]] {
            for userRepresentation in representation {
                if let item = Images(response: response, representation: userRepresentation) {
                    objects.append(item)
                }
            }
        }
        return objects
    }
}






final class Game: ResponseObjectSerializable, ResponseCollectionSerializable {
    var nextState: Int?
    var currentStateStart: Int?
    var aggregateScore: Int?
    var awayGoals: Int?
    var start: Double?
    var homeGoals: Int?
    var extraTimeHasHappened: Bool?
    var homeTeam: Team?
    var awayTeam: Team?
    var dismissals: Dismissals?
    var isResult: Bool?
    var goToExtraTime: Bool?
    var venue: AnyObject?
    var limitedCoverage: Bool?
//    var outcome: Outcome?
//    var round: Round?
    var currentState: Int?
    
    init?() {
        
    }
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let Object = representation.valueForKeyPath("nextState") as? Int {
            self.nextState = Object
        } else {
            self.nextState = nil
        }
        if let Object = representation.valueForKeyPath("currentStateStart") as? Int {
            self.currentStateStart = Object
        } else {
            self.currentStateStart = nil
        }
        if let Object = representation.valueForKeyPath("aggregateScore") as? Int {
            self.aggregateScore = Object
        } else {
            self.aggregateScore = nil
        }
        if let Object = representation.valueForKeyPath("awayGoals") as? Int {
            self.awayGoals = Object
        } else {
            self.awayGoals = nil
        }
        if let Object = representation.valueForKeyPath("homeGoals") as? Int {
            self.homeGoals = Object
        } else {
            self.homeGoals = nil
        }
        if let Object = representation.valueForKeyPath("nextState") as? Int {
            self.nextState = Object
        } else {
            self.nextState = nil
        }
        if let Object = representation.valueForKeyPath("homeTeam") {
            self.homeTeam = Team(response: response, representation: Object)
        } else {
            self.homeTeam = nil
        }
        if let Object = representation.valueForKeyPath("awayTeam") {
            self.awayTeam = Team(response: response, representation: Object)
        } else {
            self.awayTeam = nil
        }
        if let Object = representation.valueForKeyPath("start") as? Double {
            self.start = Object
        } else {
            self.start = nil
        }
    }
    
    internal class func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Game] {
        var objectsList = [Game]()
        let objectArray = representation as! [AnyObject]
        for o in objectArray {
            if let object = Game(response: response, representation: o) {
                objectsList.append(object)
            }
        }
        return objectsList
    }
}

final class Team: ResponseObjectSerializable {
    var name: String?
    var shortName: String?
    var shortCode: String?
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let Object = representation.valueForKeyPath("name") as? String? {
            self.name = Object
        } else {
            self.name = nil
        }
        if let Object = representation.valueForKeyPath("shortName") as? String? {
            self.shortName = Object
        } else {
            self.shortName = nil
        }
        if let Object = representation.valueForKeyPath("shortCode") as? String? {
            self.shortCode = Object
        } else {
            self.shortCode = nil
        }
    }
}

final class Dismissals: ResponseObjectSerializable {
    var home: Int?
    var away: Int?
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let Object = representation.valueForKeyPath("home") as? Int? {
            self.home = Object
        } else {
            self.home = nil
        }
        if let Object = representation.valueForKeyPath("away") as? Int? {
            self.away = Object
        } else {
            self.away = nil
        }
    }
}

//final class Round: ResponseObjectSerializable {
//    var home: Int?
//    var away: Int?
//    
//    init?(response: NSHTTPURLResponse, representation: AnyObject) {
//        if let Object = representation.valueForKeyPath("home") as? Int? {
//            self.home = Object
//        } else {
//            self.home = nil
//        }
//        if let Object = representation.valueForKeyPath("away") as? Int? {
//            self.away = Object
//        } else {
//            self.away = nil
//        }
//    }
//}

final class Outcome: ResponseObjectSerializable {
    var home: Int?
    var away: Int?
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let Object = representation.valueForKeyPath("home") as? Int? {
            self.home = Object
        } else {
            self.home = nil
        }
        if let Object = representation.valueForKeyPath("away") as? Int? {
            self.away = Object
        } else {
            self.away = nil
        }
    }
}
