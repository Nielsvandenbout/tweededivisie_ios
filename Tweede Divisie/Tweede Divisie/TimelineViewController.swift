//
//  TimelineViewController.swift
//  Tweede Divisie
//
//  Created by Niels van den Bout on 30-09-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit
import TwitterKit

class TimelineViewController: TWTRTimelineViewController, TWTRTweetViewDelegate {
    
    convenience init() {
        let dataSource = TWTRSearchTimelineDataSource(searchQuery: "#barhhc", APIClient: TWTRAPIClient())
        self.init(dataSource: dataSource)
        
        // Set the title for Nav bar
        self.title = "@\(dataSource.searchQuery)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let client = TWTRAPIClient()
        self.dataSource = TWTRSearchTimelineDataSource(searchQuery: "barhhc", APIClient: client)
    }
}