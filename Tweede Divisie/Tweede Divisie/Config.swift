//
//  Config.swift
//  Ortho Intern
//
//  Created by Martijn Smit on 22/01/16.
//  Copyright © 2016 WeMa Mobile B.V. All rights reserved.
//

import UIKit
import Alamofire

struct Config {
    enum Router: URLRequestConvertible {
        static let baseURLString = "http://iappministrator.com/v2_tweede_divisie/"
        static let baseURLString2 = "https://api.crowdscores.com/api/v1"
        // calls
        case GetNews
        case GetRanking
        case GetLive
        
        var method: Alamofire.Method {
            return .GET
        }
        
        var path: String {
            switch self {
            case .GetNews:
                return "news/getAll"
            case .GetRanking:
                return "ranking/getAll"
            case .GetLive:
                return "/matches?competition_id=330&api_key=03c8676748604bb19882b75fa214b3f7"
            }
        }
        // MARK: URLRequestConvertible
        var URLRequest: NSMutableURLRequest {
            
            
            
            let URL = NSURL(string: Router.baseURLString)!
            let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
            mutableURLRequest.HTTPMethod = method.rawValue
            
            let URL2 = NSURL(string: Router.baseURLString2)!
            let mutableURLRequest2 = NSMutableURLRequest(URL: URL2.URLByAppendingPathComponent(path))
            mutableURLRequest2.HTTPMethod = method.rawValue
            
            switch self {
                case .GetLive:
                    return mutableURLRequest2
                default:
                    return mutableURLRequest
            }
        }
    }
}
