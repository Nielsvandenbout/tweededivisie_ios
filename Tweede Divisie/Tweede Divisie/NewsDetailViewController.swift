//
//  NewsDetailViewController.swift
//  Tweede Divisie
//
//  Created by Niels van den Bout on 14-09-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit
import Haneke

class NewsDetailViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsLabel: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var teamOneImageView: UIImageView!
    @IBOutlet weak var teamTwoImageView: UIImageView!
    
    let NAVBAR_CHANGE_POINT: CGFloat = 20
    var newsItem = DataItems()
    var teamsItems = [DataItems]()
    var team1Image = ""
    var team2Image = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        setupViews()
    }
    
    func setupViews() {
        setupNavBar()
        setupNewsData()
    }
    
    func setupNavBar() {
        navigationController?.navigationBar.backgroundColor = UIColor.clearColor()
        navigationController?.navigationBar.barTintColor = UIColor.clearColor()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.translucent = true
        navigationController?.navigationBarHidden = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "action_button"), style: .Plain, target: self, action: #selector(showAction))

    }
    
    
    func setupNewsData() {
        if newsItem != nil {
            newsLabel.text = newsItem?.content
            newsTitle.text = newsItem?.title
            author.text = newsItem?.author
            newsImage.image = UIImage()
            if let url = newsItem!.attachments?.images.first?.original {
                setupImage(url, superView: newsImage, contentMode: .ScaleAspectFill)
            } else {
                
            }
        }
        setupTeamImages()
       
    }
    
    func setupImage(url: String, superView: UIImageView, contentMode: UIViewContentMode) {
        newsImage.hnk_setImageFromURL(NSURL(fileURLWithPath: url))
        let URL = NSURL(string: url)!
        let cache = Shared.imageCache
        let fetcher = NetworkFetcher<UIImage>(URL: URL)
        cache.fetch(fetcher: fetcher).onSuccess { image in
            superView.image = image
            superView.clipsToBounds = true
        }
        cache.fetch(fetcher: fetcher).onFailure({ (error) in
            
        })

    }
    
    func setTeamImages(superViewOne: UIImageView, superViewTwo: UIImageView, teams: [String]) {
        for team in teamsItems {
            if team.teamName != nil {
                if team.teamName == teams[0] {
                    if let url = team.attachments?.images.first?.original {
                        setupImage(url, superView: superViewOne, contentMode: .ScaleAspectFit)
                    }
                }
                if teams.count == 2 {
                    if team.teamName == teams[1] {
                        if let url = team.attachments?.images.first?.original {
                            setupImage(url, superView: superViewTwo, contentMode: .ScaleAspectFit)
                        }
                    }
                }
            }
        }
    }
    
    func setupTeamImages() {
        if let tagsArray = newsItem!.tags?.componentsSeparatedByString(", ") {
            if tagsArray.count > 0 {
                setTeamImages(teamOneImageView, superViewTwo: teamTwoImageView, teams: tagsArray)
            }
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time( DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), closure)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let color = UIColor(red: 15/255, green: 175/255, blue: 0/255, alpha: 1.0)
        let offsetY = scrollView.contentOffset.y
        
        if (offsetY > NAVBAR_CHANGE_POINT) {
            let alpha = CGFloat (min(1, 1 - ((NAVBAR_CHANGE_POINT + 64 - offsetY) / 64)))
            if navigationController != nil {
            self.navigationController!.navigationBar.lt_setBackgroundColor(color.colorWithAlphaComponent(alpha))
            }
        } else {
            if navigationController != nil {
            self.navigationController!.navigationBar.lt_setBackgroundColor(color.colorWithAlphaComponent(0))
            }
        }

        
    }
    
    func showAction() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        alertController.addAction(UIAlertAction(title: "Artikel delen", style: .Default, handler: { (UIAlertAction) in
            
        }))
        alertController.addAction(UIAlertAction(title: "Annuleer", style: .Cancel, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return (self.navigationController?.navigationBarHidden)!
    }
}
