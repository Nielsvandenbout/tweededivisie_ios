//
//  HomeViewController.swift
//  Tweede Divisie
//
//  Created by Niels van den Bout on 14-09-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit
import Haneke
import Auk
import moa
import Firebase
import FirebaseDatabase

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIScrollViewDelegate, UITabBarDelegate {

    @IBOutlet weak var segmentedBackground: UIView!
    @IBOutlet weak var newsTableView: UITableView!
    @IBOutlet weak var ImagescrollView: UIScrollView!
    @IBOutlet weak var overlay: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    
    var rootRef: FIRDatabaseReference!
    var newsItems = [DataItems]()
    var newsItem = DataItems()
    var teamsItems = [DataItems]()
    var teamItem = DataItems()
    var teams = [DataItems]()
    var items = DataItems()
    var tag = 0
    var more = ["Info", "Meer van WeMa Mobile", "Instellingen"]
    var games = [Game]()
    var game = Game()
    
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if item.tag == 0 {
            tag = item.tag
            newsTableView.tableHeaderView = sliderView
            downloadData()
            addRefreshControl()
        } else if item.tag == 1 {
            tag = item.tag
            newsTableView.tableHeaderView = UIView()
            newsTableView.reloadData()
            downloadData()
            addRefreshControl()
        } else if item.tag == 2 {
            tag = item.tag
            newsTableView.tableHeaderView = UIView()
            newsTableView.reloadData()
            downloadData()
            addRefreshControl()
        } else if item.tag == 3 {
            tag = item.tag
            removeRefreshControl()
            newsTableView.tableHeaderView = UIView()
            newsTableView.reloadData()
        }
    }
    
    @IBAction func tappedScrollView(sender: AnyObject) {
        newsItem = newsItems[ImagescrollView.auk.currentPageIndex!]
        performSegueWithIdentifier("showNewsDetail", sender: self)
    }
    
    func setupViews() {
        setupNavBar()
        setupSegmentedView()
        setupNewsTableView()
        downloadNewsData()

        addRefreshControl()
        items = DataItems(played: "P", won: "W", draw: "G", lost: "V", goalsScored: "DP", goalsAgainst: "", goalsDifference: "", points: "P", rank: "POS", rankDifference: "")
        ImagescrollView.delegate = self
        ImagescrollView.userInteractionEnabled = true
        title = "Tweede Divisie"
        
        let setting = UIUserNotificationSettings(forTypes: .Alert, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(setting)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        tabBar.delegate = self
        tabBar.tintColor = UIColor(red: 15/255, green: 175/255, blue: 0/255, alpha: 1.0)
        tabBar.selectedItem = tabBar.items?.first
        
        for item in tabBar.items! {
            item.setTitleTextAttributes([NSFontAttributeName: UIFont(name:"Lato-regular", size:11)!],
                                        forState: .Normal)
        }
        downloadGames()
        configureDatabase()
    }
    
    func setupScrollView() {
        overlay.clipsToBounds = true
        
        ImagescrollView.auk.settings.placeholderImage = UIImage()
        ImagescrollView.auk.removeAll()
        ImagescrollView.auk.settings.pageControl.visible = false
        
        for item in newsItems {
            ImagescrollView.auk.show(url: (item.attachments?.images.first?.original)!)
        }
        if ImagescrollView.auk.currentPageIndex == 0 {
            titleLabel.text = newsItems.first?.title
        }
        Moa.settings.cache.requestCachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if newsItems.count != 0 || ImagescrollView.auk.currentPageIndex != nil {
            titleLabel.text = newsItems[ImagescrollView.auk.currentPageIndex!].title
        }
    }
    
    func downloadData() {
        if tag == 0 {
            newsTableView.separatorStyle = .None
            if newsItems.count == 0 {
                downloadNewsData()
            } else {
                self.newsTableView.reloadData()
            }
        } else if tag == 1 {
            newsTableView.separatorStyle = .SingleLine
            newsTableView.separatorInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
            if teamsItems.count == 0 {
                downloadTeamsData()
            } else {
                self.newsTableView.reloadData()
            }
        } else if tag == 2 {
            newsTableView.separatorStyle = .SingleLine
            newsTableView.separatorInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
            if teamsItems.count == 0 {
                downloadGames()
            } else {
                self.newsTableView.reloadData()
            }
        } else {
            newsTableView.separatorStyle = .SingleLine
            newsTableView.separatorInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)
            self.newsTableView.reloadData()
        }
    }
    
    func downloadNewsData() {
        API.sharedInstance.getNews { (responseObject, error) in
            if responseObject != nil {
                if responseObject?.response != nil {
                    if responseObject?.response?.dataItems != nil {
                        self.newsItems.removeAll()
                        self.newsItems = (responseObject?.response?.dataItems)!
                        self.downloadTeamsData()
                        self.setupScrollView()
                        self.refreshControl.endRefreshing()
                    }
                }
            }
        }
    }
    
    func downloadGames() {
        var set = Set<String>()
        API.sharedInstance.getMatches { (responseObject, error) in
            if responseObject != nil {
                for item in responseObject! {
                    set.insert(item.awayTeam!.shortCode!)
                }
                for item in set {
                    print(item)
                }
                self.games.removeAll()
                self.games = responseObject!
                self.setupScrollView()
                self.newsTableView.reloadData()
                self.refreshControl.endRefreshing()
            } else {
                print(error)
            }
        }
    }
    
    func formatDate(timeInterval: Double) -> String {
        
        //Convert to Date
        let date = NSDate(timeIntervalSince1970: timeInterval)
        
        //Date formatting
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd, MMMM yyyy HH:mm:a"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let dateString = dateFormatter.stringFromDate(date)
        return dateString
    }
    
    func downloadTeamsData() {
        items = DataItems(played: "P", won: "W", draw: "G", lost: "V", goalsScored: "DP", goalsAgainst: "", goalsDifference: "", points: "P", rank: "POS", rankDifference: "")
        API.sharedInstance.getRanking { (responseObject, error) in
            if responseObject != nil {
                if responseObject?.response != nil {
                    if responseObject?.response?.dataItems != nil {
                        self.teamsItems.removeAll()
                        self.teamsItems.append(self.items!)
                        for team in (responseObject?.response?.dataItems)! {
                            self.teamsItems.append(team)
                        }
                        self.teams = (responseObject?.response?.dataItems)!
                        if self.teamsItems.count == (responseObject?.response?.dataItems.count)! + 1 {
                            self.newsTableView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    }
                }
            }
        }
    }
    
    func reloadData() {
        if tag == 0 {
            downloadNewsData()
        } else if tag == 1 || tag == 2 {
            downloadTeamsData()
        }
    }
    
    func addRefreshControl() {
        if refreshControl.superview == nil {
            refreshControl.backgroundColor = UIColor(red: 222/255, green: 222/255, blue: 222/255, alpha: 1.0)
            refreshControl.addTarget(self, action: #selector(reloadData), forControlEvents: UIControlEvents.ValueChanged)
            newsTableView.addSubview(refreshControl)
        }
    }
    
    func removeRefreshControl() {
        refreshControl.removeFromSuperview()
    }
    
    func setupNewsTableView() {
        newsTableView.delegate = self
        newsTableView.dataSource = self
        newsTableView.tableFooterView = UIView()
        newsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 55, right: 0)
    }
    
    func setupSegmentedView() {
        let attr = NSDictionary(object: UIFont(name: "Lato-Regular", size: 14.0)!, forKey: NSFontAttributeName)
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject : AnyObject] , forState: .Normal)
    }
    
    func showNotiViewController() {
        
        performSegueWithIdentifier("showNotificationsViewController", sender: self)
    }
    
    func setupNavBar() {
        navigationController?.navigationBar.backgroundColor = UIColor.clearColor()
        navigationController?.navigationBar.barTintColor = UIColor.clearColor()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Lato-Bold", size: 23)!]
        
        // delete white navbar line
        navigationController?.navigationBar.shadowImage = UIImage()
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
        fixedSpace.width = -7
        let notiIcon = UIBarButtonItem(image: UIImage(named:"notificationIcon"), style: .Plain, target: self, action: #selector(showNotiViewController))
        navigationItem.rightBarButtonItems = [fixedSpace, notiIcon]
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tag == 0 {
            return newsItems.count
        } else if tag == 1 {
            return teamsItems.count
        } else if tag == 2 {
//            if teamsItems.count > 0 {
//                return 1
//            } else {
//                return 0
//            }
            return games.count
        } else {
            return more.count
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tag == 0 {
            newsItem = newsItems[indexPath.row]
            performSegueWithIdentifier("showNewsDetail", sender: self)
        } else if tag == 3 {
            if indexPath.row == 0 {
                performSegueWithIdentifier("showInfo", sender: self)
            } else if indexPath.row == 1 {
                if let url = NSURL(string: "https://itunes.apple.com/nl/developer/wema-mobile-b.v./id494886354") {
                    UIApplication.sharedApplication().openURL(url)
                }
            } else {
                if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.sharedApplication().openURL(appSettings)
                }
            }
        } else if tag == 2 {
            game = games[indexPath.row]
            performSegueWithIdentifier("showGameInfo", sender: self)
        }
        newsTableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // check for segmented control
        if tag == 0  {
            let cell = newsTableView.dequeueReusableCellWithIdentifier("newsCell") as! NewsCell
            
            cell.newsTitle.text = newsItems[indexPath.row].title
            let publishDate = newsItems[indexPath.row].publishDate
            if let tagsArray = newsItems[indexPath.row].tags?.componentsSeparatedByString(", ") {
                if tagsArray.count > 0 {
                    setTeamImages(cell.team1Image, superViewTwo: cell.team2Image, teams: tagsArray)
                }
            }
            
            cell.dateLabel.text = publishDate.chopSuffix(9)
            cell.contentLabel.text = newsItems[indexPath.row].content
            cell.newsImage.image = UIImage()
            if let url = newsItems[indexPath.row].attachments?.images.first?.original {
                showImage(url, imageView: cell.newsImage, contentMode: .ScaleAspectFill)
            } else {

            }
            return cell
        } else if tag == 1 {
            let cell = newsTableView.dequeueReusableCellWithIdentifier("teamCell") as! TeamCell
            
            cell.teamName.text = teamsItems[indexPath.row].teamName
            cell.teamImage.image = UIImage()
            cell.teamGames.text = teamsItems[indexPath.row].played
            cell.teamWins.text = teamsItems[indexPath.row].won
            cell.teamDraws.text = teamsItems[indexPath.row].draw
            cell.teamLosses.text = teamsItems[indexPath.row].lost
            cell.teamRank.text = teamsItems[indexPath.row].rank
            if indexPath.row != 0 {
                cell.teamGoals.text = "\(teamsItems[indexPath.row].goalsScored):\(teamsItems[indexPath.row].goalsAgainst)"
            } else {
                cell.teamGoals.text = "\(teamsItems[indexPath.row].goalsScored)"
            }
            cell.teamPoints.text = teamsItems[indexPath.row].points

            if teamsItems[indexPath.row].rankDifference == "0" {
                cell.rankDifference.image = UIImage(named: "sameplace")
            } else if teamsItems[indexPath.row].rankDifference == "1" {
                cell.rankDifference.image = UIImage(named: "arrowdown")
            } else if teamsItems[indexPath.row].rankDifference == "2" {
                cell.rankDifference.image = UIImage(named: "arrowup")
            }
            
            // 0 = gelijk
            // 1 = omlaag
            // 2 = omhoog
            
            if let url = teamsItems[indexPath.row].attachments?.images.first?.original {
                showImage(url, imageView: cell.teamImage, contentMode: .ScaleAspectFit)
            } else {

            }
            return cell
        } else if tag == 2 {
            let cell = newsTableView.dequeueReusableCellWithIdentifier("liveCell") as! LiveCell
            let awayTeam = setTeamName(games[indexPath.row].awayTeam!.name!)
            let homeTeam = setTeamName(games[indexPath.row].homeTeam!.name!)
            cell.awayTeam.text = awayTeam
            cell.homeTeam.text = homeTeam
            
            cell.awayTeamImage.image = UIImage()
            cell.homeTeamImage.image = UIImage()
            
            setTeamImages(cell.homeTeamImage, superViewTwo: cell.awayTeamImage, teams: [homeTeam, awayTeam])
            
            cell.liveScore.text = "\(games[indexPath.row].homeGoals!):\(games[indexPath.row].awayGoals!)"
            return cell
        } else {
            let cell = newsTableView.dequeueReusableCellWithIdentifier("moreCell") as! MoreCell
            cell.itemLabel.text = more[indexPath.row]
            return cell
        }
    }
    
    func setTeamName(name: String) -> String {
        if name == "BVV Barendrecht" {
            return "Barendrecht"
        } else if name == "Lienden" {
            return "FC Lienden"
        } else if name == "Jong Sparta Rotterdam" {
            return "Jong Sparta"
        } else if name == "Amsterdamsche" {
            return "AFC"
        } else if name == "Jong AZ Alkmaar" {
            return "Jong AZ"
        } else if name == "Jong Twente" {
            return "Jong FC Twente"
        } else if name == "Koninklijke" {
            return "Koninklijke HFC"
        } else {
            return name
        }
    }
    
    
    func configureDatabase() {
        rootRef = FIRDatabase.database().reference()
        rootRef.child("matches").observeEventType(.ChildAdded, withBlock: { snapshot in
            if snapshot.childrenCount < 0 {
                print("bier")
            }
            print(snapshot.value)
        })
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tag == 0 {
            return 160
        } else if tag == 1 {
            return 32
        } else {
            return 38
        }
    }
    
    func showImage(url: String, imageView: UIImageView, contentMode: UIViewContentMode) {
        imageView.hnk_setImageFromURL(NSURL(fileURLWithPath: url))
        let URL = NSURL(string: url)!

        let cache = Shared.imageCache
        let fetcher = NetworkFetcher<UIImage>(URL: URL)
        cache.fetch(fetcher: fetcher).onSuccess { image in
            imageView.image = image
            imageView.contentMode = contentMode
            imageView.clipsToBounds = true
        }
        cache.fetch(fetcher: fetcher).onFailure({ (error) in
            
        })

    }
    
    func setTeamImages(superViewOne: UIImageView, superViewTwo: UIImageView, teams: [String]) {
        for team in teamsItems {
            if team.teamName != nil {
                if team.teamName == teams[0] {
                    if let url = team.attachments?.images.first?.original {
                        showImage(url, imageView: superViewOne, contentMode: .ScaleAspectFit)
                    }
                }
                if teams.count == 2 {
                    if team.teamName == teams[1] {
                        if let url = team.attachments?.images.first?.original {
                            showImage(url, imageView: superViewTwo, contentMode: .ScaleAspectFit)
                        }
                    }
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showNewsDetail" {
            if let destinationVC = segue.destinationViewController as? NewsDetailViewController {
                destinationVC.newsItem = newsItem
                destinationVC.teamsItems = teams
            }
        }
        if segue.identifier == "showNotificationsViewController" {
            if let destinationVC = segue.destinationViewController as? SetNotificationsViewController {
                destinationVC.teamsItems = teams
            }
        }
        if segue.identifier == "showGameInfo" {
            if let destinationVC = segue.destinationViewController as? GameViewController {
                destinationVC.teamsItems = teams
                destinationVC.game = game
            }
        }
    }
    
}

class NewsCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var team1Image: UIImageView!
    @IBOutlet weak var team2Image: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        backView.layer.shadowColor = UIColor.blackColor().CGColor
        backView.layer.shadowOffset = CGSizeZero
        backView.layer.shadowOpacity = 0.5
        backView.layer.shadowRadius = 5
        backView.layer.cornerRadius = 5
        newsImage.layer.cornerRadius = 3
        newsImage.clipsToBounds = true
        backView.clipsToBounds = true
    }
    
}

class TeamCell: UITableViewCell {
    
    @IBOutlet weak var rankDifference: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamPoints: UILabel!
    @IBOutlet weak var teamGoals: UILabel!
    @IBOutlet weak var teamGames: UILabel!
    @IBOutlet weak var teamLosses: UILabel!
    @IBOutlet weak var teamDraws: UILabel!
    @IBOutlet weak var teamWins: UILabel!
    @IBOutlet weak var teamRank: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    


}

class LiveCell: UITableViewCell {
    @IBOutlet weak var homeTeam: UILabel!
    @IBOutlet weak var awayTeam: UILabel!
    @IBOutlet weak var homeTeamImage: UIImageView!
    @IBOutlet weak var awayTeamImage:UIImageView!
    @IBOutlet weak var liveScore: UILabel!
    @IBOutlet weak var liveScoreView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        liveScoreView.layer.cornerRadius = 3
        liveScoreView.clipsToBounds = true
    }
}

class MoreCell: UITableViewCell {
    @IBOutlet weak var itemLabel: UILabel!
}

